
package com.rosin.tucn.ngi.systems.ros2ar.app.ros.rosapi.message;

import com.rosin.tucn.ngi.systems.ros2ar.app.ros.message.Message;
import com.rosin.tucn.ngi.systems.ros2ar.app.ros.message.MessageType;

@MessageType(string = "rosapi/Topic")
public class Topic extends Message {
    public String topic;
    
    public Topic() {}
    
    public Topic(String topic) {
        this.topic = topic;
    }
}
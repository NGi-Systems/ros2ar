
package com.rosin.tucn.ngi.systems.ros2ar.app.ros.rosbridge.operation;

import com.rosin.tucn.ngi.systems.ros2ar.app.ros.message.MessageType;

@MessageType(string = "unsubscribe")
public class Unsubscribe extends Operation {
    public String topic;

    public Unsubscribe() {}
    
    public Unsubscribe(String topic) {
        this.topic = topic;
    }    
}

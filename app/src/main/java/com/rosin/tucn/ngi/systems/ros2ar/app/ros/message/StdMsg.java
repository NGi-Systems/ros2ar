package com.rosin.tucn.ngi.systems.ros2ar.app.ros.message;

@MessageType(string = "std_msgs/String")
public class StdMsg extends Message {
    public String data;
}


package com.rosin.tucn.ngi.systems.ros2ar.app.ros.rosbridge.operation;

import com.rosin.tucn.ngi.systems.ros2ar.app.ros.message.MessageType;
import com.rosin.tucn.ngi.systems.ros2ar.app.ros.rosbridge.indication.Indicated;
import com.rosin.tucn.ngi.systems.ros2ar.app.ros.rosbridge.indication.Indicator;

@MessageType(string = "wrapper")
public class Wrapper extends Operation {
    @Indicator
    public String op;
    @Indicated
    public Operation msg;
    
    public Wrapper() {}
}

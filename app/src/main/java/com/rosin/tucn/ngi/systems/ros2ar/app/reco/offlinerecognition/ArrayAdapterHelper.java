package com.rosin.tucn.ngi.systems.ros2ar.app.reco.offlinerecognition;

import android.widget.ArrayAdapter;

import java.util.ArrayList;

/**
 * Created by JDV on 8/23/2017.
 */

public class ArrayAdapterHelper<T> {
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public void update(ArrayAdapter arrayAdapter, ArrayList<T> listOfObject){
        arrayAdapter.clear();
        for (T object : listOfObject){
            arrayAdapter.add(object);
        }
    }
}

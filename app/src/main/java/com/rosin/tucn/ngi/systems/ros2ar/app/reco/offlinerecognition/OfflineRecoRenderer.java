/*===============================================================================
Copyright (c) 2016 PTC Inc. All Rights Reserved.

Copyright (c) 2012-2014 Qualcomm Connected Experiences, Inc. All Rights Reserved.

Vuforia is a trademark of PTC Inc., registered in the United States and other 
countries.
===============================================================================*/

package com.rosin.tucn.ngi.systems.ros2ar.app.reco.offlinerecognition;

import android.opengl.GLSurfaceView;
import android.util.Log;

import com.rosin.tucn.ngi.systems.ros2ar.app.AppRenderer;
import com.rosin.tucn.ngi.systems.ros2ar.app.AppRendererControl;
import com.rosin.tucn.ngi.systems.ros2ar.app.ApplicationSession;
import com.rosin.tucn.ngi.systems.ros2ar.app.MonitoringInterface;
import com.rosin.tucn.ngi.systems.ros2ar.app.utils.Texture;
import com.vuforia.Device;
import com.vuforia.State;
import com.vuforia.TrackableResult;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;


// The renderer class for the OfflineReco sample.
public class OfflineRecoRenderer implements GLSurfaceView.Renderer, AppRendererControl {
    private static final String LOGTAG = "OfflineRecoRenderer";

    private ApplicationSession vuforiaAppSession;
    private OfflineReco mActivity;
    private AppRenderer mAppRenderer;

    private Vector<Texture> mTextures;

    private int shaderProgramID;
    private int vertexHandle;
    private int textureCoordHandle;
    private int mvpMatrixHandle;
    private int texSampler2DHandle;

    private float kBuildingScale = 0.012f;

    private boolean mIsActive = false;
    private boolean mModelIsLoaded = false;

    private List<MonitoringInterface> listeners = new ArrayList<>();

    private static final float OBJECT_SCALE_FLOAT = 0.003f;

    public OfflineRecoRenderer(OfflineReco activity, ApplicationSession session) {
        mActivity = activity;
        vuforiaAppSession = session;
        // AppRenderer used to encapsulate the use of RenderingPrimitives setting
        // the device mode AR/VR and stereo mode
        mAppRenderer = new AppRenderer(this, mActivity, Device.MODE.MODE_AR, false, 0.01f, 5f);
    }


    // Called to draw the current frame.
    @Override
    public void onDrawFrame(GL10 gl) {
        if (!mIsActive)
            return;

        // Call our function to render content from AppRenderer class
        mAppRenderer.render();
    }


    public void setActive(boolean active) {
        mIsActive = active;

        if (mIsActive)
            mAppRenderer.configureVideoBackground();
    }


    // Called when the surface is created or recreated.
    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        Log.d(LOGTAG, "GLRenderer.onSurfaceCreated");

        // Call Vuforia function to (re)initialize rendering after first use
        // or after OpenGL ES context was lost (e.g. after onPause/onResume):
        vuforiaAppSession.onSurfaceCreated();

        mAppRenderer.onSurfaceCreated();
    }


    // Called when the surface changed size.
    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {
        Log.d(LOGTAG, "GLRenderer.onSurfaceChanged");

        // Call Vuforia function to handle render surface size changes:
        vuforiaAppSession.onSurfaceChanged(width, height);

        // RenderingPrimitives to be updated when some rendering change is done
        mAppRenderer.onConfigurationChanged(mIsActive);


    }

    public void updateConfiguration() {
        mAppRenderer.onConfigurationChanged(mIsActive);
    }

    // The render function called from SampleAppRendering by using RenderingPrimitives views.
    // The state is owned by AppRenderer which is controlling it's lifecycle.
    // State should not be cached outside this method.
    public void renderFrame(State state, float[] projectionMatrix) {
        // Renders video background replacing Renderer.DrawVideoBackground()
        if (!OfflineReco.isTransparentScreen) {
            mAppRenderer.renderVideoBackground();
        }

        //
        //GLES20.glEnable(GLES20.GL_CULL_FACE);

        // Did we find any trackables this frame?
        if (state.getNumTrackableResults() > 0) {
            // Gets current trackable result
            TrackableResult trackableResult = state.getTrackableResult(0);

            if (trackableResult == null) {
                return;
            }

            mActivity.stopFinderIfStarted();
            Log.d("IMAGE RECO", "Image was recognized!");
            sendNotification(trackableResult.getTrackable().getName()); //numele robotului va fi returnat
            // Renders the Augmentation View with the 3D Book data Panel
            //renderAugmentation(trackableResult, projectionMatrix);

        } else {
            sendNotification("");
            mActivity.startFinderIfStopped();
        }

        //GLES20.glDisable(GLES20.GL_DEPTH_TEST);

        //Renderer.getInstance().end();

    }

  /*  private void printUserData(Trackable trackable) {
        String userData = (String) trackable.getUserData();
        Log.d(LOGTAG, "UserData:Retreived User Data	\"" + userData + "\"");
    }
*/

    public void setTextures(Vector<Texture> textures) {
        mTextures = textures;

    }

    public void sendNotification(String recognizedTarget) {
        for (MonitoringInterface listener : listeners) {
            listener.notification(recognizedTarget);
        }
    }

    public void addListener(MonitoringInterface listener) {
        listeners.add(listener);
    }

}

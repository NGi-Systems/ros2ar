
package com.rosin.tucn.ngi.systems.ros2ar.app.ros.rosapi.message;

import com.rosin.tucn.ngi.systems.ros2ar.app.ros.message.Message;
import com.rosin.tucn.ngi.systems.ros2ar.app.ros.message.MessageType;
import com.rosin.tucn.ngi.systems.ros2ar.app.ros.message.TimePrimitive;


@MessageType(string = "rosapi/GetTimeResponse")
public class GetTime extends Message {
    public TimePrimitive time;
}

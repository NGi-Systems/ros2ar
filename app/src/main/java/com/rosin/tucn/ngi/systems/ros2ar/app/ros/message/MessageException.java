
package com.rosin.tucn.ngi.systems.ros2ar.app.ros.message;

public class MessageException extends Exception {

    public MessageException(String message) {
        super(message);
    }
    
    public MessageException(String message, Throwable cause) {
        super(message, cause);
    }    
}


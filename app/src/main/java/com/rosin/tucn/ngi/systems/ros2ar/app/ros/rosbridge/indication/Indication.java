
package com.rosin.tucn.ngi.systems.ros2ar.app.ros.rosbridge.indication;

import java.lang.reflect.Field;

public class Indication {
    public static boolean isIndicated(Field f) {
        return (f.getAnnotation(Indicated.class) != null);
    }
    
    public static boolean asArray(Field f) {
        return (f.getAnnotation(AsArray.class) != null);
    }
    
    public static boolean isBase64Encoded(Field f) {
        return ((f.getAnnotation(Base64Encoded.class) != null) &&
                f.getType().isArray() &&
                f.getType().getComponentType().equals(byte.class)); 
    }
    
    public static String getIndicatorName(Class c) {
        return getName(c, Indicator.class);
    }
    
    public static String getIndicatedName(Class c) {
        return getName(c, Indicated.class);
    }
    
    private static String getName(Class c, Class annotation) {
        String result = null;
        for (Field f : c.getFields()) {
            if (f.getAnnotation(annotation) != null) {
                result = f.getName();
                break;
            }
        }
        return result;
    }

}

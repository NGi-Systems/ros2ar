
package com.rosin.tucn.ngi.systems.ros2ar.app.ros.rosbridge.operation;

import com.rosin.tucn.ngi.systems.ros2ar.app.ros.message.MessageType;

@MessageType(string = "unadvertise")
public class Unadvertise extends Operation {
    public String topic;

    public Unadvertise() {}
    
    public Unadvertise(String topic) {
        this.topic = topic;
    }    
}

package com.rosin.tucn.ngi.systems.ros2ar.app.ros.message;


@MessageType(string = "std_msgs/Int16MultiArray")
public class AudioMsg extends Message {
    public short[] data;
}

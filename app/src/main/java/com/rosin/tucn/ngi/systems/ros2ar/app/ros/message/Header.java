
package com.rosin.tucn.ngi.systems.ros2ar.app.ros.message;

@MessageType(string = "std_msgs/Header")
public class Header extends Message {
    public long seq;
    public TimePrimitive stamp;
    public String frame_id;
}

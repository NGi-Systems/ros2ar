
package com.rosin.tucn.ngi.systems.ros2ar.app.ros.rosbridge.operation;

import com.rosin.tucn.ngi.systems.ros2ar.app.ros.message.Message;
import com.rosin.tucn.ngi.systems.ros2ar.app.ros.message.MessageType;
import com.rosin.tucn.ngi.systems.ros2ar.app.ros.rosbridge.indication.AsArray;
import com.rosin.tucn.ngi.systems.ros2ar.app.ros.rosbridge.indication.Indicated;
import com.rosin.tucn.ngi.systems.ros2ar.app.ros.rosbridge.indication.Indicator;

@MessageType(string = "call_service")
public class CallService extends Operation {
    @Indicator
    public String service;
    @Indicated
    @AsArray
    public Message args;
    public Integer fragment_size; // use Integer for optional items
    public String compression;

    public CallService() {}
    
    public CallService(String service, Message args) {
        this.service = service;
        this.args = args;
    }    
}

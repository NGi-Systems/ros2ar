
package com.rosin.tucn.ngi.systems.ros2ar.app.ros.rosbridge.operation;

import com.rosin.tucn.ngi.systems.ros2ar.app.ros.message.MessageType;

@MessageType(string = "status")
public class Status extends Operation {
    String level;
    String msg;
    
    public Status() {}
    
    public Status(String level, String msg) {
        this.level = level;
        this.msg = msg;
    }
}

/*===============================================================================
Copyright (c) 2016 PTC Inc. All Rights Reserved.


Copyright (c) 2012-2014 Qualcomm Connected Experiences, Inc. All Rights Reserved.

Vuforia is a trademark of PTC Inc., registered in the United States and other 
countries.
===============================================================================*/

package com.rosin.tucn.ngi.systems.ros2ar.app.reco.offlinerecognition;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Color;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.rosin.tucn.ngi.systems.ros2ar.app.ApplicationControl;
import com.rosin.tucn.ngi.systems.ros2ar.app.ApplicationException;
import com.rosin.tucn.ngi.systems.ros2ar.app.ApplicationSession;
import com.rosin.tucn.ngi.systems.ros2ar.app.MonitoringInterface;
import com.rosin.tucn.ngi.systems.ros2ar.app.utils.LoadingDialogHandler;
import com.rosin.tucn.ngi.systems.ros2ar.app.utils.SampleApplicationGLView;
import com.rosin.tucn.ngi.systems.ros2ar.app.utils.Texture;
import com.rosin.tucn.ngi.systems.ros2ar.app.model.Equipment;
import com.rosin.tucn.ngi.systems.ros2ar.app.model.Node;
import com.rosin.tucn.ngi.systems.ros2ar.app.model.RobotType;
import com.rosin.tucn.ngi.systems.ros2ar.app.model.RobotTypes;
import com.rosin.tucn.ngi.systems.ros2ar.app.ros.rosbridge.RosBridgeLaunchClient;
import com.rosin.tucn.ngi.systems.ros2ar.app.ui.AppMenu.AppMenu;
import com.rosin.tucn.ngi.systems.ros2ar.app.ui.AppMenu.AppMenuGroup;
import com.rosin.tucn.ngi.systems.ros2ar.app.ui.AppMenu.AppMenuInterface;
import com.vuforia.CameraDevice;
import com.vuforia.DataSet;
import com.vuforia.ObjectTracker;
import com.vuforia.STORAGE_TYPE;
import com.vuforia.State;
import com.vuforia.TargetFinder;
import com.vuforia.Trackable;
import com.vuforia.Tracker;
import com.vuforia.TrackerManager;
import com.vuforia.Vuforia;
import com.rosin.tucn.ngi.systems.ros2ar.demo.R;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Vector;


public class OfflineReco extends Activity implements ApplicationControl,
        AppMenuInterface, MonitoringInterface {
    private static final String LOGTAG = "OfflineReco";
    public static final String RECONNECT = "reconnect";
    public static final String CONTINUE = "continue";
    public static final String ON = "ON";
    public static final String OFF = "OFF";
    public static final String CAN_T_DISCONNECT = "Can't Disconnect";
    public static final String DONT_GET_NODES = "Dont get Nodes";
    public static final String CONNECTED = "Connected";
    public static final String NOT_CONNECTED = "Not Connected";
    public static final String ROSAPI_NODES = "/rosapi/nodes";

    private static final String ROBOT_TYPES = "ros.json";

    private static final String ROBOT_OVERVIEW = "robots";


    private static final String SECTION_ORDER_OVERVIEW = "orderOverview";

    private static final String ROS2AR_DEVICE_TARGET_DB = "ros2ar-device-target-db.xml";
    private static final float ORDER_OVERVIEW_SCREEN_RATIO = 0.75f;

    ApplicationSession vuforiaAppSession;

    private DataSet mCurrentDataset;
    private int mCurrentDatasetSelectionIndex = 0;

    private int mStartDatasetsIndex = 0;
    private int mDatasetsNumber = 0;
    private ArrayList<String> mDatasetStrings = new ArrayList<>();

    // Our OpenGL view:
    private SampleApplicationGLView mGlView;

    // Our renderer:
    private OfflineRecoRenderer mRenderer;

    // The textures we will use for rendering:
    private Vector<Texture> mTextures;

    private boolean mSwitchDatasetAsap = false;
    private boolean mFlash = false;
    private boolean mContAutofocus = false;
    private boolean mExtendedTracking = false;

    private boolean mFinderStarted = false;

    // declare scan line and its animation
    private View scanLine;
    private TranslateAnimation scanAnimation;

    private View mFlashOptionView;

    private RelativeLayout mUILayout;

    private TextView equipmentStatus;
    private TextView equipmentName;
    private ImageView recognizedEquipmentIcon;
    private TextView nodesTotal;
    private ListView equipmentContentListView;
    private LinearLayout headerLayout;
    private LinearLayout equipmentOverviewLayout;
    private TextView headerNodeStatus;
    private TextView headerNodeName;
    private EquipmentContentArrayAdapter equipmentArrayAdapter;
    private LinearLayout.LayoutParams nodesOverViewParams;

    private AppMenu mAppMenu;

    LoadingDialogHandler loadingDialogHandler = new LoadingDialogHandler(this);

    // Alert Dialog used to display SDK errors
    private AlertDialog mErrorDialog;

    private RobotTypes robotTypes;

    boolean mIsDroidDevice = false;

    public static boolean isTransparentScreen = true;

    // call the rosBridgeClient
    private String[][] nodeDataArray = new String[][]{{}, {}};
    private List<String> basicNodesChecklist = new ArrayList<>();
    private List<String> basicNodesList = new ArrayList<>();
    private RosBridgeLaunchClient rosBridgeLaunchClient;
    private int check;
    private String nextStep;
    private int firstConnect = 0;
    private Equipment equipment;
    private int recognizedEquipmentIconIndex;

    //TODO ROS clean
    // TODO aliniat
    // TODO poza sa apare in functie de tip
    // TODO zip
    // TODO doc pt ROS
    // TODO ROS code repo

    // Called when the activity first starts or the user navigates back to an
    // activity.
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(LOGTAG, "onCreate");
        super.onCreate(savedInstanceState);

        vuforiaAppSession = new ApplicationSession(this);

        startLoadingAnimation();
        mDatasetStrings.add(ROS2AR_DEVICE_TARGET_DB);

        vuforiaAppSession
                .initAR(this, ActivityInfo.SCREEN_ORIENTATION_SENSOR);

        mIsDroidDevice = Build.MODEL.toLowerCase().startsWith("droid");
        jsonToPojo(ROBOT_TYPES, ROBOT_OVERVIEW);
        activateImmersiveMode();
        countDownTimer.start();
    }

    // Process Single Tap event to trigger autofocus
    private class GestureListener extends
            GestureDetector.SimpleOnGestureListener {
        // Used to set autofocus one second after a manual focus is triggered
        private final Handler autofocusHandler = new Handler();


        @Override
        public boolean onDown(MotionEvent e) {
            return true;
        }

        @Override
        public boolean onSingleTapUp(MotionEvent e) {
            // Generates a Handler to trigger autofocus
            // after 1 second
            autofocusHandler.postDelayed(new Runnable() {
                public void run() {
                    boolean result = CameraDevice.getInstance().setFocusMode(
                            CameraDevice.FOCUS_MODE.FOCUS_MODE_TRIGGERAUTO);

                    if (!result)
                        Log.e("SingleTapUp", "Unable to trigger focus");
                }
            }, 1000L);

            return true;
        }
    }

    private void activateImmersiveMode() {
        View decorView = getWindow().getDecorView();
        // Hide both the navigation bar and the status bar.
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        decorView.setSystemUiVisibility(uiOptions);
    }

    // Called when the activity will start interacting with the user.
    @Override
    protected void onResume() {
        Log.d(LOGTAG, "onResume");
        super.onResume();

        // This is needed for some Droid devices to force portrait
        if (mIsDroidDevice) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }

        try {
            vuforiaAppSession.resumeAR();
        } catch (ApplicationException e) {
            Log.e(LOGTAG, e.getString());
        }

        // Resume the GL view:
        if (mGlView != null) {
            mGlView.setVisibility(View.VISIBLE);
            mGlView.onResume();
        }

    }


    // Callback for configuration changes the activity handles itself
    @Override
    public void onConfigurationChanged(Configuration config) {
        Log.d(LOGTAG, "onConfigurationChanged");
        super.onConfigurationChanged(config);

        vuforiaAppSession.onConfigurationChanged();
    }


    // Called when the system is about to start resuming a previous activity.
    @Override
    protected void onPause() {
        Log.d(LOGTAG, "onPause");
        super.onPause();

        if (mGlView != null) {
            mGlView.setVisibility(View.INVISIBLE);
            mGlView.onPause();
        }

        try {
            vuforiaAppSession.pauseAR();
        } catch (ApplicationException e) {
            Log.e(LOGTAG, e.getString());
        }
    }


    // The final call you receive before your activity is destroyed.
    @Override
    protected void onDestroy() {
        Log.d(LOGTAG, "onDestroy");
        super.onDestroy();

        try {
            vuforiaAppSession.stopAR();
        } catch (ApplicationException e) {
            Log.e(LOGTAG, e.getString());
        }


        System.gc();
    }


    // Initializes AR application components.
    private void initApplicationAR() {
        // Create OpenGL ES view:
        int depthSize = 16;
        int stencilSize = 0;
        boolean translucent = Vuforia.requiresAlpha();

        mGlView = new SampleApplicationGLView(this);
        mGlView.init(translucent, depthSize, stencilSize);

        mRenderer = new OfflineRecoRenderer(this, vuforiaAppSession);
        mRenderer.setTextures(mTextures);
        mGlView.setRenderer(mRenderer);
        mRenderer.addListener(this);
    }


    private void startLoadingAnimation() {
        // Inflates the Overlay Layout to be displayed above the Camera View
        LayoutInflater inflater = LayoutInflater.from(this);
        mUILayout = (RelativeLayout) inflater.inflate(R.layout.offline_reco_layout,
                null, false);

        mUILayout.setVisibility(View.VISIBLE);
        mUILayout.setBackgroundColor(Color.BLACK);

        // By default
        loadingDialogHandler.mLoadingDialogContainer = mUILayout
                .findViewById(R.id.loading_indicator);
        loadingDialogHandler.mLoadingDialogContainer
                .setVisibility(View.VISIBLE);

        scanLine = mUILayout.findViewById(R.id.scan_line);

        nodesOverViewParams = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT);

        equipmentOverviewLayout = (LinearLayout) mUILayout.findViewById(R.id.equipmentOverviewLayout);


        equipmentStatus = (TextView) mUILayout.findViewById(R.id.tableStatus);
        equipmentName = (TextView) mUILayout.findViewById(R.id.tableName);
        equipmentContentListView = (ListView) mUILayout.findViewById(R.id.equipmentContentListView);
        equipmentContentListView.setFocusable(false);
        recognizedEquipmentIcon = (ImageView) mUILayout.findViewById(R.id.equipment_type_image_view);
        nodesTotal = (TextView) mUILayout.findViewById(R.id.nodesTotal);
        headerLayout = (LinearLayout) mUILayout.findViewById(R.id.headerLayout);
        headerLayout.setVisibility(View.GONE);

        headerNodeStatus = (TextView) mUILayout.findViewById(R.id.headerEquipmentItemStatus);
        headerNodeName = (TextView) mUILayout.findViewById(R.id.headerEquipmentItemDetails);


        setEquipmentOverviewAndDashboardVisibility(SECTION_ORDER_OVERVIEW, View.INVISIBLE);

        scanLine.setVisibility(View.GONE);
        scanAnimation = new TranslateAnimation(
                TranslateAnimation.ABSOLUTE, 0f,
                TranslateAnimation.ABSOLUTE, 0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 1.0f);
        scanAnimation.setDuration(4000);
        scanAnimation.setRepeatCount(-1);
        scanAnimation.setRepeatMode(Animation.REVERSE);
        scanAnimation.setInterpolator(new LinearInterpolator());

        equipmentArrayAdapter = new EquipmentContentArrayAdapter(this, R.layout.equipment_overview_row_layout, new ArrayList<Node>());
        equipmentContentListView.setAdapter(equipmentArrayAdapter);

        addContentView(mUILayout, new LayoutParams(LayoutParams.MATCH_PARENT,
                LayoutParams.MATCH_PARENT));
    }


    // Methods to load and destroy tracking data.
    @Override
    public boolean doLoadTrackersData() {
        TrackerManager tManager = TrackerManager.getInstance();
        ObjectTracker objectTracker = (ObjectTracker) tManager
                .getTracker(ObjectTracker.getClassType());
        if (objectTracker == null)
            return false;

        if (mCurrentDataset == null)
            mCurrentDataset = objectTracker.createDataSet();

        if (mCurrentDataset == null)
            return false;

        if (!mCurrentDataset.load(
                mDatasetStrings.get(mCurrentDatasetSelectionIndex),
                STORAGE_TYPE.STORAGE_APPRESOURCE))
            return false;

        if (!objectTracker.activateDataSet(mCurrentDataset))
            return false;

        int numTrackables = mCurrentDataset.getNumTrackables();
        for (int count = 0; count < numTrackables; count++) {
            Trackable trackable = mCurrentDataset.getTrackable(count);
            if (isExtendedTrackingActive()) {
                trackable.startExtendedTracking();
            }

            String name = "Current Dataset : " + trackable.getName();
            trackable.setUserData(name);
            Log.d(LOGTAG, "UserData:Set the following user data "
                    + (String) trackable.getUserData());
        }

        return true;
    }


    @Override
    public boolean doUnloadTrackersData() {
        // Indicate if the trackers were unloaded correctly
        boolean result = true;

        TrackerManager tManager = TrackerManager.getInstance();
        ObjectTracker objectTracker = (ObjectTracker) tManager
                .getTracker(ObjectTracker.getClassType());
        if (objectTracker == null)
            return false;

        if (mCurrentDataset != null && mCurrentDataset.isActive()) {
            if (objectTracker.getActiveDataSet(0).equals(mCurrentDataset)
                    && !objectTracker.deactivateDataSet(mCurrentDataset)) {
                result = false;
            } else if (!objectTracker.destroyDataSet(mCurrentDataset)) {
                result = false;
            }

            mCurrentDataset = null;
        }

        return result;
    }


    @Override
    public void onInitARDone(ApplicationException exception) {

        if (exception == null) {
            initApplicationAR();

            mRenderer.setActive(true);

            // Now add the GL surface view. It is important
            // that the OpenGL ES surface view gets added
            // BEFORE the camera is started and video
            // background is configured.
            addContentView(mGlView, new LayoutParams(LayoutParams.MATCH_PARENT,
                    LayoutParams.MATCH_PARENT));

            // Sets the UILayout to be drawn in front of the camera
            mUILayout.bringToFront();

            // Sets the layout background to transparent
            mUILayout.setBackgroundColor(Color.TRANSPARENT);

            try {
                vuforiaAppSession.startAR(CameraDevice.CAMERA_DIRECTION.CAMERA_DIRECTION_DEFAULT);
            } catch (ApplicationException e) {
                Log.e(LOGTAG, e.getString());
            }

            boolean result = CameraDevice.getInstance().setFocusMode(
                    CameraDevice.FOCUS_MODE.FOCUS_MODE_CONTINUOUSAUTO);

            if (result)
                mContAutofocus = true;
            else
                Log.e(LOGTAG, "Unable to enable continuous autofocus");

            mAppMenu = new AppMenu(this, this, "Image Targets",
                    mGlView, mUILayout, null);
            setSampleAppMenuSettings();

        } else {
            Log.e(LOGTAG, exception.getString());
            showInitializationErrorMessage(exception.getString());
        }
    }

    public void stopFinderIfStarted() {
        if (mFinderStarted) {
            mFinderStarted = false;

            // Get the object tracker:
            TrackerManager trackerManager = TrackerManager.getInstance();
            ObjectTracker objectTracker = (ObjectTracker) trackerManager
                    .getTracker(ObjectTracker.getClassType());

            // Initialize orderContent finder:
            TargetFinder targetFinder = objectTracker.getTargetFinder();

            targetFinder.stop();
            scanlineStop();
        }
    }

    private void scanlineStart() {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                scanLine.setVisibility(View.GONE);
                scanLine.setAnimation(scanAnimation);
            }
        });
    }

    private void scanlineStop() {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                scanLine.setVisibility(View.GONE);
                scanLine.clearAnimation();
            }
        });
    }


    // Shows initialization error messages as System dialogs
    public void showInitializationErrorMessage(String message) {
        final String errorMessage = message;
        runOnUiThread(new Runnable() {
            public void run() {
                if (mErrorDialog != null) {
                    mErrorDialog.dismiss();
                }

                // Generates an Alert Dialog to show the error message
                AlertDialog.Builder builder = new AlertDialog.Builder(
                        OfflineReco.this);
                builder
                        .setMessage(errorMessage)
                        .setTitle(getString(R.string.INIT_ERROR))
                        .setCancelable(false)
                        .setIcon(0)
                        .setPositiveButton(getString(R.string.button_OK),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        finish();
                                    }
                                });

                mErrorDialog = builder.create();
                mErrorDialog.show();
            }
        });
    }


    @Override
    public void onVuforiaUpdate(State state) {
        if (mSwitchDatasetAsap) {
            mSwitchDatasetAsap = false;
            TrackerManager tm = TrackerManager.getInstance();
            ObjectTracker ot = (ObjectTracker) tm.getTracker(ObjectTracker
                    .getClassType());
            if (ot == null || mCurrentDataset == null
                    || ot.getActiveDataSet(0) == null) {
                Log.d(LOGTAG, "Failed to swap datasets");
                return;
            }

            doUnloadTrackersData();
            doLoadTrackersData();
        }
    }


    @Override
    public boolean doInitTrackers() {
        // Indicate if the trackers were initialized correctly
        boolean result = true;

        TrackerManager tManager = TrackerManager.getInstance();
        Tracker tracker;

        // Trying to initialize the image tracker
        tracker = tManager.initTracker(ObjectTracker.getClassType());
        if (tracker == null) {
            Log.e(
                    LOGTAG,
                    "Tracker not initialized. Tracker already initialized or the camera is already started");
            result = false;
        } else {
            Log.i(LOGTAG, "Tracker successfully initialized");
        }
        return result;
    }


    @Override
    public boolean doStartTrackers() {
        // Indicate if the trackers were started correctly
        boolean result = true;

        Tracker objectTracker = TrackerManager.getInstance().getTracker(
                ObjectTracker.getClassType());
        if (objectTracker != null)
            objectTracker.start();

        return result;
    }


    @Override
    public boolean doStopTrackers() {
        // Indicate if the trackers were stopped correctly
        boolean result = true;

        Tracker objectTracker = TrackerManager.getInstance().getTracker(
                ObjectTracker.getClassType());
        if (objectTracker != null)
            objectTracker.stop();

        return result;
    }


    @Override
    public boolean doDeinitTrackers() {
        // Indicate if the trackers were deinitialized correctly
        boolean result = true;

        TrackerManager tManager = TrackerManager.getInstance();
        tManager.deinitTracker(ObjectTracker.getClassType());

        return result;
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        // Process the Gestures
      /*  if (mAppMenu != null && mAppMenu.processEvent(event))
            return true;

        return mGestureDetector.onTouchEvent(event);*/
        return false;
    }


    boolean isExtendedTrackingActive() {
        return mExtendedTracking;
    }

    final public static int CMD_BACK = -1;
    final public static int CMD_EXTENDED_TRACKING = 1;
    final public static int CMD_AUTOFOCUS = 2;
    final public static int CMD_FLASH = 3;
    final public static int CMD_CAMERA_FRONT = 4;
    final public static int CMD_CAMERA_REAR = 5;
    final public static int CMD_DATASET_START_INDEX = 6;
    final public static int CMD_TRANSPARENT_SCREEN = 7;


    // This method sets the menu's settings
    private void setSampleAppMenuSettings() {
        AppMenuGroup group;

        group = mAppMenu.addGroup("", false);
        group.addTextItem(getString(R.string.menu_back), -1);

        group = mAppMenu.addGroup("", true);
        group.addSelectionItem(getString(R.string.menu_extended_tracking),
                CMD_EXTENDED_TRACKING, false);

        CameraInfo ci = new CameraInfo();
        boolean deviceHasFrontCamera = false;
        boolean deviceHasBackCamera = false;
        for (int i = 0; i < Camera.getNumberOfCameras(); i++) {
            Camera.getCameraInfo(i, ci);
            if (ci.facing == CameraInfo.CAMERA_FACING_FRONT)
                deviceHasFrontCamera = true;
            else if (ci.facing == CameraInfo.CAMERA_FACING_BACK)
                deviceHasBackCamera = true;
        }

        group = mAppMenu
                .addGroup(getString(R.string.menu_datasets), true);
        mStartDatasetsIndex = CMD_DATASET_START_INDEX;
        mDatasetsNumber = mDatasetStrings.size();

        // Add datasets to the configuration
        group.addRadioItem("NextMenu - Offline Data", mStartDatasetsIndex, true);
        mAppMenu.attachMenu();
    }


    @Override
    public boolean menuProcess(int command) {

        boolean result = true;

        switch (command) {
            case CMD_BACK:
                finish();
                break;

            case CMD_FLASH:
                result = CameraDevice.getInstance().setFlashTorchMode(!mFlash);

                if (result) {
                    mFlash = !mFlash;
                } else {
                    showToast(getString(mFlash ? R.string.menu_flash_error_off
                            : R.string.menu_flash_error_on));
                    Log.e(LOGTAG,
                            getString(mFlash ? R.string.menu_flash_error_off
                                    : R.string.menu_flash_error_on));
                }
                break;

            case CMD_AUTOFOCUS:

                if (mContAutofocus) {
                    result = CameraDevice.getInstance().setFocusMode(
                            CameraDevice.FOCUS_MODE.FOCUS_MODE_NORMAL);

                    if (result) {
                        mContAutofocus = false;
                    } else {
                        showToast(getString(R.string.menu_contAutofocus_error_off));
                        Log.e(LOGTAG,
                                getString(R.string.menu_contAutofocus_error_off));
                    }
                } else {
                    result = CameraDevice.getInstance().setFocusMode(
                            CameraDevice.FOCUS_MODE.FOCUS_MODE_CONTINUOUSAUTO);

                    if (result) {
                        mContAutofocus = true;
                    } else {
                        showToast(getString(R.string.menu_contAutofocus_error_on));
                        Log.e(LOGTAG,
                                getString(R.string.menu_contAutofocus_error_on));
                    }
                }

                break;

            case CMD_CAMERA_FRONT:
            case CMD_CAMERA_REAR:

                // Turn off the flash
                if (mFlashOptionView != null && mFlash) {
                    // OnCheckedChangeListener is called upon changing the checked state
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                        ((Switch) mFlashOptionView).setChecked(false);
                    } else {
                        ((CheckBox) mFlashOptionView).setChecked(false);
                    }
                }

                vuforiaAppSession.stopCamera();

                try {
                    vuforiaAppSession
                            .startAR(command == CMD_CAMERA_FRONT ? CameraDevice.CAMERA_DIRECTION.CAMERA_DIRECTION_FRONT
                                    : CameraDevice.CAMERA_DIRECTION.CAMERA_DIRECTION_BACK);

                    mRenderer.updateConfiguration();

                } catch (ApplicationException e) {
                    showToast(e.getString());
                    Log.e(LOGTAG, e.getString());
                    result = false;
                }
                doStartTrackers();
                break;

            case CMD_EXTENDED_TRACKING:
                for (int tIdx = 0; tIdx < mCurrentDataset.getNumTrackables(); tIdx++) {
                    Trackable trackable = mCurrentDataset.getTrackable(tIdx);

                    if (!mExtendedTracking) {
                        if (!trackable.startExtendedTracking()) {
                            Log.e(LOGTAG,
                                    "Failed to start extended tracking orderContent");
                            result = false;
                        } else {
                            Log.d(LOGTAG,
                                    "Successfully started extended tracking orderContent");
                        }
                    } else {
                        if (!trackable.stopExtendedTracking()) {
                            Log.e(LOGTAG,
                                    "Failed to stop extended tracking orderContent");
                            result = false;
                        } else {
                            Log.d(LOGTAG,
                                    "Successfully started extended tracking orderContent");
                        }
                    }
                }

                if (result)
                    mExtendedTracking = !mExtendedTracking;

                break;

            default:
                if (command >= mStartDatasetsIndex
                        && command < mStartDatasetsIndex + mDatasetsNumber) {
                    mSwitchDatasetAsap = true;
                    mCurrentDatasetSelectionIndex = command
                            - mStartDatasetsIndex;
                }
                break;

            case CMD_TRANSPARENT_SCREEN:
                isTransparentScreen = false;
                break;
        }

        return result;
    }

    public void startFinderIfStopped() {
        if (!mFinderStarted) {
            mFinderStarted = true;

            // Get the object tracker:
            TrackerManager trackerManager = TrackerManager.getInstance();
            ObjectTracker objectTracker = (ObjectTracker) trackerManager
                    .getTracker(ObjectTracker.getClassType());

            // Initialize orderContent finder:
            TargetFinder targetFinder = objectTracker.getTargetFinder();

            targetFinder.clearTrackables();
            targetFinder.startRecognition();
            scanlineStart();
        }
    }

    CountDownTimer countDownTimer = new CountDownTimer(10000, 4000) {
        @Override
        public void onTick(long millisUntilFinished) {
            nextStep = CONTINUE;
        }

        @Override
        public void onFinish() {
            nextStep = RECONNECT;
            this.start();
        }
    };

    // connect to ROS With IP and Port
    private void initROSBridge(String ip, String port) {
        try {
            rosBridgeLaunchClient = new RosBridgeLaunchClient("ws://" + ip + ":" + port);
            rosBridgeLaunchClient.connect();
            Log.d(LOGTAG, CONNECTED);
        } catch (Exception e) {
            Log.d(LOGTAG, NOT_CONNECTED);
        }
    }

    private void getNodes() {
        try {
            nodeDataArray[0] = rosBridgeLaunchClient.getNodes(ROSAPI_NODES); // get the nodes which currently run in device
        } catch (Exception e) {
            Log.d(LOGTAG, DONT_GET_NODES);
        }
        basicNodesList = Arrays.asList(nodeDataArray[0]);
        Collections.sort(basicNodesList);

        try {
            // get the nodes which must run in device with a pacer created in python
            nodeDataArray[1] = rosBridgeLaunchClient.getNodesFromLaunch(robotTypes.getRobotTypes().get(recognizedEquipmentIconIndex).getPkg() + "/" + robotTypes.getRobotTypes().get(recognizedEquipmentIconIndex).getLaunchFile());

        } catch (Exception e) {
            Log.d(LOGTAG, DONT_GET_NODES);
        }
        basicNodesChecklist = Arrays.asList(nodeDataArray[1]);
        Collections.sort(basicNodesChecklist);
        rosBridgeLaunchClient.disconnect(); // disconnect from ROS
    }


    @Override
    public void notification(final String recognizedTarget) {
        final String message = recognizedTarget;
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if ("".equals(message)) {
                    try {
                        rosBridgeLaunchClient.disconnect();
                    } catch (Exception e) {
                        Log.e(message, CAN_T_DISCONNECT);
                    }
                    setEquipmentOverviewAndDashboardVisibility(SECTION_ORDER_OVERVIEW, View.INVISIBLE);
                    firstConnect = 0;
                } else {
                    if (recognizedTarget != null) {
                        List<RobotType> robotTypesList = robotTypes.getRobotTypes();
                        if (firstConnect == 0) {
                            for (int i = 0; i < robotTypesList.size(); i++) { // find in the robot type that is currently tracked
                                if (recognizedTarget.equals(robotTypesList.get(i).getQrID())) {
                                    recognizedEquipmentIconIndex = i;
                                }
                            }
                            // if it the first time when need to connect to ROS
                            // connect to ROS
                            initROSBridge(robotTypes.getRobotTypes().get(recognizedEquipmentIconIndex).getIp(), robotTypes.getRobotTypes().get(recognizedEquipmentIconIndex).getPort());
                            // get the node lists
                            getNodes();
                            firstConnect = 1;
                        } else if (RECONNECT.equals(nextStep)) {  // if it the time is elapsed re-connect to ROS
                            initROSBridge(robotTypes.getRobotTypes().get(recognizedEquipmentIconIndex).getIp(), robotTypes.getRobotTypes().get(recognizedEquipmentIconIndex).getPort());
                            getNodes();
                        }

                        equipment = checkNodesStatus(basicNodesChecklist, basicNodesList); // the checked node list we put in equipment to display
                        nodesOverViewParams.weight = ORDER_OVERVIEW_SCREEN_RATIO;
                        equipmentOverviewLayout.setLayoutParams(nodesOverViewParams);
                        displayEquipmantInfo(equipment);
                    }
                }
            }
        });
    }

    private Equipment checkNodesStatus(List<String> basicNodesChecklist, List<String> basicNodesList) {
        check = 0;
        Equipment equipment = new Equipment();
        List<Node> itemList = new ArrayList<>(basicNodesChecklist.size());
        for (int i = 0; i < basicNodesChecklist.size(); i++) {
            boolean nodeFound = false;
            Node item = new Node();
            for (int j = 0; j < basicNodesList.size(); j++) {
                if (basicNodesChecklist.get(i).equals(basicNodesList.get(j))) {
                    check++;
                    nodeFound = true;
                    item.setName(basicNodesChecklist.get(i));
                    item.setStatus(ON);
                }
            }
            if (!nodeFound) {
                item.setName(basicNodesChecklist.get(i));
                item.setStatus(OFF);
            }
            itemList.add(item);
        }
        equipment.setNodeList(itemList);
        equipment.setGoodNodes(check);

        equipment.setName(robotTypes.getRobotTypes().get(recognizedEquipmentIconIndex).getQrID());
        return equipment;
    }

    private void jsonToPojo(String jsonFilename, String classType) {
        Gson gson = new Gson();
        BufferedReader reader;
        try {
            reader = new BufferedReader(new InputStreamReader(getAssets().open(jsonFilename), "UTF-8"));
             if (ROBOT_OVERVIEW.equals(classType)) {
                robotTypes = gson.fromJson(reader, RobotTypes.class);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void displayEquipmantInfo(Equipment equipment) {
        String equipmentFullName = equipment.getName();
        List<Node> nodeList = new ArrayList<>();
        int count, size;
        Integer images[] = {R.drawable.arm, R.drawable.cnc, R.drawable.elevator};

        if (!equipment.equals(null)) {
            setEquipmentOverviewAndDashboardVisibility(SECTION_ORDER_OVERVIEW, View.VISIBLE);
            count = equipment.getGoodNodes();
            size = equipment.getNodeList().size();
            for (Node node : equipment.getNodeList()) {
                nodeList.add(node);

            }

            recognizedEquipmentIcon.setImageResource(images[recognizedEquipmentIconIndex]);
            equipmentStatus.setText("Nodes: " + count + " / " + size);
            new ArrayAdapterHelper<Node>().update(equipmentArrayAdapter, (ArrayList) nodeList);
            equipmentArrayAdapter.notifyDataSetChanged();
        }

        equipmentName.setText(equipmentFullName);

    }

    private void setEquipmentOverviewAndDashboardVisibility(String section, int visibility) {
        if (SECTION_ORDER_OVERVIEW.equals(section)) {
            headerLayout.setVisibility(visibility);
            recognizedEquipmentIcon.setVisibility(visibility);
            equipmentStatus.setVisibility(visibility);
            equipmentName.setVisibility(visibility);
            nodesTotal.setVisibility(visibility);
            equipmentContentListView.setVisibility(visibility);
            headerNodeStatus.setVisibility(visibility);
            headerNodeName.setVisibility(visibility);
        }
    }

    private void showToast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }

}

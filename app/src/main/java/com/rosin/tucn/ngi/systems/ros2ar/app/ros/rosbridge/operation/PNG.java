
package com.rosin.tucn.ngi.systems.ros2ar.app.ros.rosbridge.operation;

import com.rosin.tucn.ngi.systems.ros2ar.app.ros.message.MessageType;

@MessageType(string = "png")
public class PNG extends Operation {
    public String data;
    public Integer num;     // use Integer for optional items
    public Integer total;   // use Integer for optional items
    
    public PNG() {}
    
    public PNG(String data) {
        this.data = data;
    }
    
    public PNG(String data, int num, int total) {
        this.data = data;
        this.num = num;
        this.total = total;
    }
}

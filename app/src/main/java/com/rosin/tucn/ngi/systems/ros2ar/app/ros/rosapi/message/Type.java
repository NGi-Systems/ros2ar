
package com.rosin.tucn.ngi.systems.ros2ar.app.ros.rosapi.message;

import com.rosin.tucn.ngi.systems.ros2ar.app.ros.message.Message;
import com.rosin.tucn.ngi.systems.ros2ar.app.ros.message.MessageType;

@MessageType(string = "rosapi/Type")
public class Type extends Message {
    public String type;
    
    public Type() {}
    
    public Type(String type) {
        this.type = type;
    }
}

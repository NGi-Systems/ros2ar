
package com.rosin.tucn.ngi.systems.ros2ar.app.ros.rosbridge.operation;

import com.rosin.tucn.ngi.systems.ros2ar.app.ros.message.Message;
import com.rosin.tucn.ngi.systems.ros2ar.app.ros.message.MessageType;
import com.rosin.tucn.ngi.systems.ros2ar.app.ros.rosbridge.indication.Indicated;
import com.rosin.tucn.ngi.systems.ros2ar.app.ros.rosbridge.indication.Indicator;

@MessageType(string = "service_response")
public class ServiceResponse extends Operation {
    @Indicator
    public String service;
    public boolean result;
    @Indicated
    public Message values;

    public ServiceResponse() {}
    
    public ServiceResponse(String service) {
        this.service = service;
    }    
}

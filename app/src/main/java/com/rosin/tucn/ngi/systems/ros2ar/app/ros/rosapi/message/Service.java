
package com.rosin.tucn.ngi.systems.ros2ar.app.ros.rosapi.message;

import com.rosin.tucn.ngi.systems.ros2ar.app.ros.message.Message;
import com.rosin.tucn.ngi.systems.ros2ar.app.ros.message.MessageType;

@MessageType(string = "rosapi/Service")
public class Service extends Message {
    public String service;
    
    public Service() {}
    
    public Service(String service) {
        this.service = service;
    }
}
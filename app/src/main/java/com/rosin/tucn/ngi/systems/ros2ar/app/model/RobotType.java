package com.rosin.tucn.ngi.systems.ros2ar.app.model;

public class RobotType{
    private  String robotType="";
    private  String qrID="";
    private  String ip="";
    private  String port="";
    private  String pkg="";
    private  String launchFile="";

    public String getRobotType() {
        return robotType;
    }

    public void setRobotType(String robotType) {
        this.robotType = robotType;
    }

    public String getQrID() {
        return qrID;
    }

    public void setQrID(String qrID) {
        this.qrID = qrID;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getPkg() {
        return pkg;
    }

    public void setPkg(String pkg) {
        this.pkg = pkg;
    }

    public String getLaunchFile() {
        return launchFile;
    }

    public void setLaunchFile(String launchFile) {
        this.launchFile = launchFile;
    }



    @Override
    public String toString() {
        return "robots{" +
                "robotType='" + robotType + '\'' +
                ", qrID='" + qrID + '\'' +
                ", ip='" + ip + '\'' +
                ", port='" + port + '\'' +
                ", pkg='" + pkg + '\'' +
                ", launchFile='" + launchFile + '\'' +
                '}';
    }
}

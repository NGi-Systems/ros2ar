
package com.rosin.tucn.ngi.systems.ros2ar.app.ros.rosbridge.operation;

import com.rosin.tucn.ngi.systems.ros2ar.app.ros.message.MessageType;

@MessageType(string = "set_level")
public class SetStatusLevel extends Operation {
    public String level;
    
    public SetStatusLevel() {}
    
    public SetStatusLevel(String level) {
        this.level = null;
        if ("none".equals(level) ||
                "warning".equals(level) ||
                "error".equals(level) ||
                "info".equals(level))
            this.level = level;
    }
}

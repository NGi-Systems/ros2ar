

package com.rosin.tucn.ngi.systems.ros2ar.app.ros.message;

@MessageType(string = "rosgraph_msgs/Clock")
public class Clock extends Message {
    public TimePrimitive clock;
}

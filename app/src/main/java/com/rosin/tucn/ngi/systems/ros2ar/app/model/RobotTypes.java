package com.rosin.tucn.ngi.systems.ros2ar.app.model;

import java.util.List;

public class RobotTypes {

    public List<RobotType> robots;

    public List<RobotType> getRobotTypes() {
        return robots;
    }

    public void setRobotTypes(List<RobotType> robots) {
        this.robots = robots;
    }
}


package com.rosin.tucn.ngi.systems.ros2ar.app.ros.rosbridge.operation;

import com.rosin.tucn.ngi.systems.ros2ar.app.ros.message.MessageType;

@MessageType(string = "advertise")
public class Advertise extends Operation {
    public String topic;
    public String type;

    public Advertise() {}
    
    public Advertise(String topic, String type) {
        this.topic = topic;
        this.type = type;
    }    
}

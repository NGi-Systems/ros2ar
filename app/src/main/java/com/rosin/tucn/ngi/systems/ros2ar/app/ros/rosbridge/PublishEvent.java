package com.rosin.tucn.ngi.systems.ros2ar.app.ros.rosbridge;

import com.rosin.tucn.ngi.systems.ros2ar.app.ros.rosbridge.operation.Operation;


public class PublishEvent {
    public String msg;
    public String id;
    public String name;
    public String op;


    public PublishEvent(Operation operation, String name, String content) {
        if(operation != null) {
            id = operation.id;
            op = operation.op;
        }
        this.name = name;
        msg = content;
    }
}

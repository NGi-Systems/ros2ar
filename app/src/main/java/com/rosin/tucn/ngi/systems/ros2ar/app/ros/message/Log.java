
package com.rosin.tucn.ngi.systems.ros2ar.app.ros.message;

@MessageType(string = "rosgraph_msgs/Log")
public class Log extends Message {
    public Header header;
    public byte level;
    public String name;
    public String msg;
    public String file;
    public String function;
    public long line;
    public String[] topics;
}


package com.rosin.tucn.ngi.systems.ros2ar.app.ros.rosbridge.operation;

import com.rosin.tucn.ngi.systems.ros2ar.app.ros.message.MessageType;

@MessageType(string = "fragment")
public class Fragment extends Operation {
    public String data;
    public int num;
    public int total;
    
    public Fragment() {}
    
    public Fragment(String data, int num, int total) {
        this.data = data;
        this.num = num;
        this.total = total;
    }
}

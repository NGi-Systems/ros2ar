package com.rosin.tucn.ngi.systems.ros2ar.app.reco.offlinerecognition;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.rosin.tucn.ngi.systems.ros2ar.app.model.Node;
import com.rosin.tucn.ngi.systems.ros2ar.demo.R;

import java.util.List;

/**
 * Created by JDV on 8/22/2017.
 */

public class EquipmentContentArrayAdapter extends ArrayAdapter<Node> {
    private final Context context;
    private final List<Node> orderItems;

    public EquipmentContentArrayAdapter(Context context, int resource, List<Node> objects) {
        super(context, resource, objects);
        this.context = context;
        this.orderItems = objects;
    }

    static class ViewHolder {
        protected TextView nodeStatus;
        protected TextView nodeName;
        protected LinearLayout nodeRow;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = null;
        ViewHolder holder;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.equipment_overview_row_layout, null);
            final ViewHolder viewHolder = new ViewHolder();
            viewHolder.nodeStatus = (TextView) view.findViewById(R.id.nodeItemStatus);
            viewHolder.nodeName = (TextView) view.findViewById(R.id.nodeItemDetails);
              viewHolder.nodeRow = (LinearLayout) view.findViewById(R.id.nodeItemRow);
            view.setTag(viewHolder);
        } else {
            view = convertView;
        }

        holder = (ViewHolder) view.getTag();
        Node node = orderItems.get(position);
        holder.nodeStatus.setText(node.getStatus());
        holder.nodeName.setText(node.getName());
        if ("ON".equals(node.getStatus())) {
            holder.nodeRow.setBackgroundColor(Color.GREEN);
        } else if ("OFF".equals(node.getStatus())) {
            holder.nodeRow.setBackgroundColor(Color.RED);
        }
        return view;
    }

}

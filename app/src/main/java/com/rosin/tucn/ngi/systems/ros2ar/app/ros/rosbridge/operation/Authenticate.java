
package com.rosin.tucn.ngi.systems.ros2ar.app.ros.rosbridge.operation;

import com.rosin.tucn.ngi.systems.ros2ar.app.ros.message.MessageType;

@MessageType(string = "auth")
public class Authenticate extends Operation {
    public String mac;
    public String client;
    public String dest;
    public String rand;
    public int t;
    public String level;
    public int end;
    
    public Authenticate() {}
    
    public Authenticate(
            String mac,
            String client,
            String dest,
            String rand,
            int t,
            String level,
            int end)
    {
        this.mac = mac;
        this.client = client;
        this.dest = dest;
        this.rand = rand;
        this.t = t;
        this.level = level;
        this.end = end;

        this.id = null; // even though id is on EVERY OTHER operation type
    }
    
}

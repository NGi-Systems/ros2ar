
package com.rosin.tucn.ngi.systems.ros2ar.app.ros.rosbridge.indication;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Indicate {
    // if later we want multiple indicated fields, use an int here
}

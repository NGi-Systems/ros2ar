
package com.rosin.tucn.ngi.systems.ros2ar.app.ros.message;

@MessageType(string = "time")
public class TimePrimitive extends Message {
    public int secs;  // when requesting this format from ROSbridge, it uses 'sec' (no 's')
    public int nsecs; // when requesting this format from ROSbridge, it uses 'nsec'
}

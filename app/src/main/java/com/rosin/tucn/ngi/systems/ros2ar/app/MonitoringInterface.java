package com.rosin.tucn.ngi.systems.ros2ar.app;

/**
 * Created by JDV on 7/1/2017.
 */

public interface MonitoringInterface {

    void notification(String recognizedTarget);
}


package com.rosin.tucn.ngi.systems.ros2ar.app.ros;

import com.rosin.tucn.ngi.systems.ros2ar.app.ros.message.Message;

public interface MessageHandler<T extends Message> {
    public void onMessage(T message);
}

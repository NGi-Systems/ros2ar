
package com.rosin.tucn.ngi.systems.ros2ar.app.ros.rosbridge.operation;

import com.rosin.tucn.ngi.systems.ros2ar.app.ros.message.Message;
import com.rosin.tucn.ngi.systems.ros2ar.app.ros.message.MessageType;
import com.rosin.tucn.ngi.systems.ros2ar.app.ros.rosbridge.indication.Indicated;
import com.rosin.tucn.ngi.systems.ros2ar.app.ros.rosbridge.indication.Indicator;

@MessageType(string = "publish")
public class Publish extends Operation {
    
    @Indicator
    public String topic;
    @Indicated
    public Message msg;
    
    public Publish() {}
    
    public Publish(String topic, Message msg) {
        this.topic = topic;
        this.msg = msg;
    }
}

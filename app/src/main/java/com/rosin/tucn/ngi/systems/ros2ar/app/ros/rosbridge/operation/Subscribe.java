
package com.rosin.tucn.ngi.systems.ros2ar.app.ros.rosbridge.operation;

import com.rosin.tucn.ngi.systems.ros2ar.app.ros.message.MessageType;

@MessageType(string = "subscribe")
public class Subscribe extends Operation {
    public String topic;
    public String type;
    public Integer throttle_rate;   // use Integer for optional items
    public Integer queue_length;    // use Integer for optional items
    public Integer fragment_size;   // use Integer for optional items
    public String compression;
    
    public Subscribe() {}
    
    public Subscribe(String topic, String type) {
        this.topic = topic;
        this.type = type;
    }    
}

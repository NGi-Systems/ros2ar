
package com.rosin.tucn.ngi.systems.ros2ar.app.ros.rosapi.message;

import com.rosin.tucn.ngi.systems.ros2ar.app.ros.message.Message;
import com.rosin.tucn.ngi.systems.ros2ar.app.ros.message.MessageType;

@MessageType(string = "std_msgs/Empty")
public class Empty extends Message {
}

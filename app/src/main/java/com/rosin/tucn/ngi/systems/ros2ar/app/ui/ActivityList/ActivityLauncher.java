/*===============================================================================
Copyright (c) 2016 PTC Inc. All Rights Reserved.

Copyright (c) 2012-2015 Qualcomm Connected Experiences, Inc. All Rights Reserved.

Vuforia is a trademark of PTC Inc., registered in the United States and other 
countries.
===============================================================================*/


package com.rosin.tucn.ngi.systems.ros2ar.app.ui.ActivityList;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.rosin.tucn.ngi.systems.ros2ar.demo.R;


public class ActivityLauncher extends ListActivity {

    private String mClassToLaunch;
    private String mClassToLaunchPackage;

    private String mActivitiesROS2AR[] = {"Offline Image Recognition"};

    final static String OFFLINE_RECO_ACTIVITY = "app.OfflineRecognition.OfflineReco";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                R.layout.activities_list_text_view, mActivitiesROS2AR);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        /*setContentView(R.layout.activities_list);
        setListAdapter(adapter);*/


        mClassToLaunchPackage = getPackageName();
        mClassToLaunch = mClassToLaunchPackage + "." + OFFLINE_RECO_ACTIVITY;
        startARActivity();

    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {

        mClassToLaunchPackage = getPackageName();

        switch (position) {
            case 0:
                mClassToLaunch = mClassToLaunchPackage + "." + OFFLINE_RECO_ACTIVITY;
                break;
        }

        startARActivity();
    }

    private void startARActivity() {
        Intent i = new Intent();
        i.setClassName(mClassToLaunchPackage, mClassToLaunch);
        startActivity(i);
    }
}

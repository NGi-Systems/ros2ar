package com.rosin.tucn.ngi.systems.ros2ar.app.model;

import java.util.List;

public class Equipment {
    private String name;
    private List<Node> nodeList;
    private int goodNodes;


    public int getGoodNodes() {
        return goodNodes;
    }

    public void setGoodNodes(int goodNodes) {
        this.goodNodes = goodNodes;
    }


    public List<Node> getNodeList() {
        return nodeList;
    }

    public void setNodeList(List<Node> nodeList) {
        this.nodeList = nodeList;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

package com.rosin.tucn.ngi.systems.ros2ar.app.ros.message;


@MessageType(string = "std_msgs/String")
public class SemanticRequest extends Message {
    public  SemanticRequest(String args) {
        jsonStr = args;
    }

    public String jsonStr;
}


package com.rosin.tucn.ngi.systems.ros2ar.app.ros.rosbridge;

import com.rosin.tucn.ngi.systems.ros2ar.app.ros.message.Message;

public interface FullMessageHandler<T extends Message> {
    public void onMessage(String id, T message);
}

#!/usr/bin/env python

import rospy
import rospkg
import roslaunch
from parser_help import LaunchFileParser

from lxml import etree



def main():
	rospy.init_node('ar_monitor_py_node')

	pkg='ar_monitor'
	launch='demo.launch'

	parser=LaunchFileParser(pkg,launch)
	parser.parse()

	

if __name__=='__main__':
	main()
#!/usr/bin/env python

import rospy
import rospkg
import rosnode
import os
import rospkg
import multiprocessing
from parser_help import LaunchFileParser
from ar_monitor.srv import statusInfo,statusInfoResponse,statusInfoRequest
from ar_monitor.msg import Node


node_name='ar_status_server_node'
service_name='ar_status_service'


def start_process(pkg,node_name,node_type,args):
	action_string='rosrun '+pkg+' '+node_type+' '+args+' __name:='+node_name
	print('Action string %s',action_string)
	os.system(action_string)

def process_topics(topics):
	top=[]
	top_type=[]

	for item in topics:
		top.append(item[0])
		top_type.append(item[1])


	print(top)
	print(top_type)

	return top,top_type

def createNodeMsg(nodes):
	nodeMsgs=list()
	for k,v in nodes.items():
		nodeMsg=Node()
		res=rosnode.rosnode_ping(k,max_count=1,verbose=True)
		nodeMsg.name.data=k
		nodeMsg.running.data=res
		for kk,vv in v.items():
			if kk == 'pkg':
				nodeMsg.pkg.data=vv
			elif kk == 'args':
				nodeMsg.args.data=vv
			elif kk == 'type':
				nodeMsg.type.data=vv
			elif kk == 'demo_domain':
				print('Demo_domain:'+str(vv))
				nodeMsg.demo_domain.data=vv

		nodeMsgs.append(nodeMsg)
	return nodeMsgs

def getRequiredNodeNames(nodes):
	nodeMsgs=list()
	nodeNames=[]
	for k,v in nodes.items():
		nodeMsg=Node()
		'res=rosnode.rosnode_ping(k,max_count=1,verbose=True)'
		nodeMsg.name.data=k
		nodeNames.append('/'+k)

	return nodeNames


def server_callback(req):
	pkg_name=req.arg

	response=statusInfoResponse()
	pkg=pkg_name
	launch='demo.launch'

	parser=LaunchFileParser(pkg,launch)
	nodes=dict()
	parser.parse()
	nodes=parser.get_nodes()
	'''response.required_nodes = createNodeMsg(nodes)
	response.required_node_names=getRequiredNodeNames(nodes)'''
	response.nodes = getRequiredNodeNames(nodes)


	return response

def main():
	rospy.init_node(node_name)

	server=rospy.Service(service_name,statusInfo,server_callback)


	print('Status server up . . .')
	rospy.spin()



if __name__=='__main__':
	main()




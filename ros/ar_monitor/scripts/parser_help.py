#!/usr/bin/env python

from lxml import etree
import rospkg

"""@package docstring
The LaunchFileParser class is used for parsing purposes.
More specifically, it starts from the main launch file
and parses recursively all the nested launch files
extracting the included nodes as well as all the 
information on them.
"""

class LaunchFileParser:

	def __init__(self,pkg,launch):
		""" The constructor of the LaunchFileParser
		@param pkg  the package where the entry launch file is located
		@param launch  the name of the entry launch file
		"""
		self.pkg=pkg 
		self.launch=launch 
		self.nodes=dict() 
		self.includes=dict()
		self.args=dict()
		self.rospack=rospkg.RosPack()

	def get_nodes(self):
		return self.nodes

	def get_includes(self):
		return self.includes

	def get_args(self):
		return self.args

	def get_demo_domain(self):
		return self.get_demo_domain

	def set_demo_domain(self,domain):
		self.demo_domain=domain

	def get_pkg(self):
		return self.pkg

	def get_launch(self):
		return self.launch

	def set_launch(self,launch):
		self.launch=launch

	def set_pkg(self,pkg):
		self.pkg=pkg


	def parse_node_args(self,node_args):
		while True:
			pos=node_args.find('$(arg')

			start_pos=node_args.find('$(arg')
			end_pos=node_args.find(')')

			if start_pos == -1:
				return node_args

			else:
				temp_arg_name=node_args[start_pos+len('$(arg '):end_pos]
				temp_value=self.args[temp_arg_name]

				if 'value' in temp_value:
					val=temp_value['value']
				elif 'default' in temp_value:
					val=temp_value['default']
				else:
					val=''

				node_args=node_args.replace('$(arg '+temp_arg_name+')',val)

	def parse_node_name(self,name):
		while True:
			start_pos=name.find('$(arg')
			end_pos=name.find(')')

			if start_pos != -1:
				temp_name=name[start_pos+len('$(arg '):end_pos]
				temp_value=self.args[temp_name]

				if 'value' in temp_value:
					val=self.args[temp_name]['value']
				elif 'default' in temp_value:
					val=self.args[temp_name]['default']
				else:
					val=''

				name=name.replace('$(arg '+temp_name+')',val)
			else:
				break

		return name

	def parse_node_values(self,child):
		tag_vals=dict()

		name=child.get('name')
		pkg=child.get('pkg')
		args=child.get('args')
		node_type=child.get('type')

		if name.find('$(arg') != -1:
			name=self.parse_node_name(name)

		if pkg != None:
			tag_vals['pkg']=pkg

		if args != None:
			parsed_args=self.parse_node_args(args)
			tag_vals['args']=parsed_args

		if node_type != None:
			tag_vals['type']=node_type

		return name,tag_vals

	def split_pkg_launch_name(self,text):
		pkg=text.split(' ')[1].split(')')[0]
		launch=text.split(' ')[1].split(')')[1].split('/')[2]

		return pkg,launch

	def parse_include_values(self,include):
		file=include.get('file')
	
		if file != None:
			pkg,launch=self.split_pkg_launch_name(file)
			return pkg,launch
		else:
			return None,None


	def parse_args(self,child):

		name=child.get('name')
		default=child.get('default')
		value=child.get('value')

		temp_value=str(value)

		while True:

			temp_args=dict()
			pos=temp_value.find('$(arg')
			start_pos=temp_value.find('$(arg')
			end_pos=temp_value.find(')')

			if pos == -1:
				if value != None:
					temp_args['value']=temp_value
				if default != None:
					temp_args['default']=default

				self.args[name]=temp_args
				break

			else:
				temp_arg_name=temp_value[start_pos+len('$(arg '):end_pos]
				val=str(self.args[temp_arg_name]['value'])
				temp_value=temp_value.replace('$(arg '+temp_arg_name+')',val,1)

	def parse_file(self,pkg,launch):
		path=self.rospack.get_path(pkg)
		path+='/launch/'

		tree=etree.parse(path+launch)
		root=tree.getroot()

		for child in root:
			if child.tag == 'node':
				node_name,node_tag_vals=self.parse_node_values(child)
				self.nodes[node_name]=node_tag_vals

			elif child.tag == 'include':
				temp_values=dict()
				pkg,launch=self.parse_include_values(child)

				temp_values['pkg']=pkg
				
				if child.get('demo_domain') != None:
					print('Demo domain :'+child.get('demo_domain'))
					temp_values['demo_domain']=child.get('demo_domain')

				self.includes[launch]=temp_values

			elif child.tag == 'arg':
				self.parse_args(child)

	def parse_following_files(self,pkg,launch,domain):
		path=self.rospack.get_path(pkg)
		path+='/launch/'

		tree=etree.parse(path+launch)
		root=tree.getroot()

		for child in root:
			if child.tag == 'node':
				node_name,node_tag_vals=self.parse_node_values(child)
				node_tag_vals['demo_domain']=domain
				self.nodes[node_name]=node_tag_vals

			elif child.tag == 'include':
				temp_values=dict()
				pkg,launch=self.parse_include_values(child)

				temp_values['pkg']=pkg
				
				if child.get('demo_domain') != None:
					print('Demo domain :'+child.get('demo_domain'))
					temp_values['demo_domain']=child.get('demo_domain')

				self.includes[launch]=temp_values

			elif child.tag == 'arg':
				self.parse_args(child)

		
		'''print('=====Nodes=====')
		print('There are :'+str(len(self.nodes))+' nodes!')
		print(self.nodes)
		print('===============')
		print('=====Includes=====')
		print('There are :'+str(len(self.includes))+' includes!')
		print(self.includes)
		print('==================')
		print('=====Args=====')
		print('There are :'+str(len(self.args))+' arguments!')
		print(self.args)
		print('==============')'''

	def parse(self):
		print('Initial parse!')
		self.parse_file(self.pkg,self.launch)
		print('Parsing includes')
		print(self.includes)

		for launch,temp_values in self.includes.items():
			print('Parsing Launch file :'+launch)
			self.parse_following_files(temp_values['pkg'],launch,temp_values['demo_domain'])
			self.includes.pop(launch)

		return self.get_nodes(),self.get_args()

		
			





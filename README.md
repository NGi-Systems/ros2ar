# ROS2AR - README #

[![Build Status](https://travis-ci.com/NGi-Systems/ROS2AR.svg?branch=ROS2AR-24-Prerelease-Ros1)](https://travis-ci.com/NGi-Systems/ROS2AR)

CI (Continuous Integration) configuration for ROS (Robot Operating System).

## Contents ##
1. Setup the Smart Glasses for Development
2. Install Android Studio (Windows, Mac and Linux)
3. Setup ROS2AR for Android Studio
4. Generate and install the apk on the smart glasses
5. Setup the ROS bridge with Visual Studio Code
6. Setup the Demo

### 1. Setup Moverio for Development ###
Follow the steps below to enable USB debugging.
1. Tap "Settings" - "About Device" to open the screen.
2. "Developer options" is displayed when you tap "Build number" seven times.
3. Open "Settings" - "Developer options", and then select "USB debugging".

![](doc/graphics/smart-glasses-usb-debugging.png)

### 2. Install Android Studio ###
A. Install Android Studio - Windows
* Download the latest Android Studio executable.
* Launch the installer follow the setup wizard in Android Studio and install any SDK packages that it recommends.

B. Install Android Studio - Mac
* Launch the Android Studio DMG file.
* Drag and drop Android Studio into the Applications folder, then launch Android Studio.
* Select whether you want to import previous Android Studio settings, then click OK.
* The Android Studio Setup Wizard guides you through the rest of the setup, which includes downloading Android SDK components that are required for development.

C. Install Android Studio - Linux
* Unpack the .zip file you downloaded to an appropriate location for your applications, such as within /usr/local/ for your user profile, or /opt/ for shared users. 
* To launch Android Studio, open a terminal, navigate to the android-studio/bin/ directory, and execute studio.sh. 
* Select whether you want to import previous Android Studio settings or not, then click OK.
* The Android Studio Setup Wizard guides you through the rest of the setup, which includes downloading Android SDK components that are required for development.
Required libraries for 64-bit machines:
* If you are running a 64-bit version of Ubuntu, you need to install some 32-bit libraries with the following command:
`sudo apt-get install libc6:i386 libncurses5:i386 libstdc++6:i386 lib32z1 libbz2-1.0:i386`
* If you are running 64-bit Fedora, the command is:
`sudo yum install zlib.i686 ncurses-libs.i686 bzip2-libs.i686`

### 3. Setup ROS2AR for Android Studio ###
1. Clone the ros2ar repo
2. Open the project in Android Studio
3. Build the project and wait until the indexing is done

### 4. Generate and install the apk on the smart glasses
A. Generate debug .apk
1. Open Android Studio and go to Build -> Build Bundle(s) / APK(s) -> Build APK(s)

![](doc/graphics/build-bundle-apk.png)

B. Generate signed .apk
1. Open Android Studio and go to Build ->Generate Signed Bundle / APK

![](doc/graphics/generate-signed-apk.png)
![](doc/graphics/generate-signed-apk-2.png)
![](doc/graphics/generate-signed-apk-3.png)

Enter the necessary details in order to generate a signed apk. For development or testing purposes the debug .apk fits the purpose. 

* Navigate to \\app\build\outputs\apk and here you will find the generated apk
* Considering that the smart glasses are connected to the computer, open a console and type `adb devices` and you should see a list with the devices currently connected to the machine

 ![](doc/graphics/adb-devices-win.jpg)![](doc/graphics/adb_devices_linux.png)
 
 * Install the apk on the device using the following syntax `adb -s <device-id> install <path-to-apk>`
 If you are in the folder where the .apk resides you can use something like this: `adb -s <device-id> install <name-of-the-app.apk>` e.g. `adb -s 091E002400000001 install ros2ar.apk`
 - - - -
### Acknowledgements ###
![ROSIN](doc/graphics/rosin_small.png)

Supported by ROSIN - ROS-Industrial Quality-Assured Robot Software Components.
More information: [ROSIN](http://rosin-project.eu)

![EU](doc/graphics/EU_small.png)

This project has received funding from the European Union’s Horizon 2020
research and innovation programme under grant agreement no. 732287.
